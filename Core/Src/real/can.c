#include "real/CAN.h"



void CAN_init(CAN_HandleTypeDef *hcan){


	CAN_ref = hcan;
	/* Start the CAN peripheral */
	if (HAL_CAN_Start(hcan) != HAL_OK)
	{
	  Error_Handler();
	}


	CAN_setFilter(0, 0, 0);

	if (HAL_CAN_ActivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)	{
	  Error_Handler();
	}

	HAL_NVIC_SetPriority(CAN1_RX1_IRQn,0,0);
	HAL_NVIC_EnableIRQ(CAN1_RX1_IRQn);

}

void CAN_setFilter(uint32_t id, uint32_t mask, uint8_t bank){
	uint32_t highId = (id & 0xFFFF0000) >> 16;
	uint32_t lowId = id & 0xFFFF;
	uint32_t highMask = (mask & 0xFFFF0000) >> 16;
	uint32_t lowMask = mask & 0xFFFF;
    CAN_FilterTypeDef  sFilterConfig;
	sFilterConfig.FilterBank = bank;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = highId;
	sFilterConfig.FilterIdLow = lowId;
	sFilterConfig.FilterMaskIdHigh = highMask;
	sFilterConfig.FilterMaskIdLow = lowMask;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;

	if (HAL_CAN_ConfigFilter(CAN_ref, &sFilterConfig) != HAL_OK)
	{
	  Error_Handler();
	}
}

void CAN_send(uint32_t id, unsigned char * data, uint8_t len, uint8_t request){
	/* Configure Transmission process */
	CAN_TxHeader.ExtId = id;
	if (request == 1) {
		CAN_TxHeader.RTR = CAN_RTR_REMOTE;
	} else {
		CAN_TxHeader.RTR = CAN_RTR_DATA;
	}
	CAN_TxHeader.IDE = CAN_ID_EXT; // always extended
	CAN_TxHeader.DLC = len;
	CAN_TxHeader.TransmitGlobalTime = DISABLE;


	if (HAL_CAN_AddTxMessage(CAN_ref, &CAN_TxHeader, data, &TxMailbox) != HAL_OK) {
		Error_Handler();
	}

//  uint32_t fifoFill = HAL_CAN_GetRxFifoFillLevel(&hcan, CAN_RX_FIFO0);
//  uint32_t fifoFill2 = HAL_CAN_GetRxFifoFillLevel(&hcan, CAN_RX_FIFO1);
}

void CAN_receive_loop(){
	uint8_t fifo = HAL_CAN_GetRxFifoFillLevel(CAN_ref, CAN_RX_FIFO0);
	if (fifo > 0) { // got a new message! lol that sucks a big time :D
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		if (HAL_CAN_GetRxMessage(CAN_ref, CAN_RX_FIFO0, &CAN_RxHeader, CAN_RxData) != HAL_OK) {
			Error_Handler();
		} else {
			svachuchan_receive(&CAN_RxHeader, CAN_RxData);
		}
	}
	osDelay(10); // TODO fix!
}

// this is not called :(
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *CanHandle) {
	//HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
	if (HAL_CAN_GetRxMessage(CanHandle, CAN_RX_FIFO0, &CAN_RxHeader, CAN_RxData) != HAL_OK) {
		Error_Handler();
	} else {
//		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
	}

}
