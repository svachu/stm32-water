/*
 * Flow Meter
 */

#include "real/FlowMeter.h" // https://github.com/sekdiy/FlowMeter


FlowSensorProperties UncalibratedSensor = {60.0f, 5.0f, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};



// CLEAN:
FlowSensorProperties SvachuCleanSensor = {25.0f, 5.0f, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
// RAIN: instantaneous flow pulse characteristics: F = [8.1Q-5] ± 10%, F means instantaneous pulse value (Hz), Q means instantaneous flow
FlowSensorProperties SvachuRainSensor = {45.0f, 5.0f, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};


FlowSensorProperties FS300A = {60.0f, 5.5f, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
FlowSensorProperties FS400A = {60.0f, 4.8f, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};



void FlowMeter(FlowSensorProperties prop){
	                                                   //!< store pin number
	FlowMeter__properties = SvachuCleanSensor;                                                      //!< store sensor properties
	FlowMeter2__properties = SvachuRainSensor;                                                      //!< store sensor properties


	// TODO: setup PIN and interrupt for interruptCallback and for RISING

	FlowMeter_reset();                                                          //!< ignore pulses generated during initialisation
}

double FlowMeter_getCurrentFlowrate() {
    return FlowMeter__currentFlowrate;                                          //!< in l/min
}

double FlowMeter_getCurrentVolume() {
    return FlowMeter__currentVolume;                                            //!< in l
}

double FlowMeter_getTotalFlowrate() {
    return FlowMeter__totalVolume / (FlowMeter__totalDuration / 1000.0f) * 60.0f;   //!< in l/min
}

double FlowMeter_getTotalVolume() {
    return FlowMeter__totalVolume;                                              //!< in l
}

void FlowMeter_tick(unsigned long duration) {
    /* sampling */
//    noInterrupts();                                                         //!< going to change interrupt variable(s)
//	TODO: disable interrupt
    volatile unsigned long pulses = FlowMeter__currentPulses;                   //!< sample current pulses from counter
    FlowMeter__currentPulses = 0;                                               //!< reset pulse counter after successful sampling
    volatile unsigned long pulses2 = FlowMeter2__currentPulses;                   //!< sample current pulses from counter
	FlowMeter2__currentPulses = 0;                                               //!< reset pulse counter after successful sampling
//    interrupts();                                                           //!< done changing interrupt variable(s)
//    TODO: enable interrupts

    /* normalisation */
    double seconds = duration / 1000.0f;                                    //!< normalised duration (in s, i.e. per 1000ms)
    double frequency = pulses / seconds;                                    //!< normalised frequency (in 1/s)
    double frequency2 = pulses2 / seconds;                                    //!< normalised frequency (in 1/s)



    /* determine current correction factor (from sensor properties) */
    unsigned int decile = floor(10.0f * frequency / (FlowMeter__properties.capacity * FlowMeter__properties.kFactor));          //!< decile of current flow relative to sensor capacity
    unsigned int ceiling =  9;                                                                                          //!< highest possible decile index
    unsigned int decile2 = floor(10.0f * frequency2 / (FlowMeter2__properties.capacity * FlowMeter2__properties.kFactor));          //!< decile of current flow relative to sensor capacity
	unsigned int ceiling2 =  9;                                                                                          //!< highest possible decile index


    FlowMeter__currentCorrection = FlowMeter__properties.kFactor / FlowMeter__properties.mFactor[min(decile, ceiling)];             //!< combine constant k-factor and m-factor for decile
    FlowMeter2__currentCorrection = FlowMeter2__properties.kFactor / FlowMeter2__properties.mFactor[min(decile2, ceiling2)];             //!< combine constant k-factor and m-factor for decile


    /* update current calculations: */
    FlowMeter__currentFlowrate = frequency / FlowMeter__currentCorrection;          //!< get flow rate (in l/min) from normalised frequency and combined correction factor
    FlowMeter__currentVolume = FlowMeter__currentFlowrate / (60.0f/seconds);        //!< get volume (in l) from normalised flow rate and normalised time
    FlowMeter2__currentFlowrate = frequency2 / FlowMeter2__currentCorrection;          //!< get flow rate (in l/min) from normalised frequency and combined correction factor
	FlowMeter2__currentVolume = FlowMeter2__currentFlowrate / (60.0f/seconds);        //!< get volume (in l) from normalised flow rate and normalised time



    /* update statistics: */
    FlowMeter__currentDuration = duration;                                      //!< store current tick duration (convenience, in ms)
    FlowMeter__currentFrequency = frequency;                                    //!< store current pulses per second (convenience, in 1/s)
    FlowMeter__totalDuration += duration;                                       //!< accumulate total duration (in ms)
    FlowMeter__totalVolume += FlowMeter__currentVolume;                             //!< accumulate total volume (in l)
    FlowMeter__totalCorrection += FlowMeter__currentCorrection * duration;          //!< accumulate corrections over time

    FlowMeter2__currentDuration = duration;                                      //!< store current tick duration (convenience, in ms)
    FlowMeter2__currentFrequency = frequency2;                                    //!< store current pulses per second (convenience, in 1/s)
    FlowMeter2__totalDuration += duration;                                       //!< accumulate total duration (in ms)
    FlowMeter2__totalVolume += FlowMeter2__currentVolume;                             //!< accumulate total volume (in l)
    FlowMeter2__totalCorrection += FlowMeter2__currentCorrection * duration;          //!< accumulate corrections over time
}

void FlowMeter_count() {
	FlowMeter__currentPulses++;                                                 //!< this should be called from an interrupt service routine
}

void FlowMeter_reset() {
//    noInterrupts();                                                         //!< going to change interrupt variable(s)
	FlowMeter__currentPulses = 0;                                               //!< reset pulse counter
	FlowMeter2__currentPulses = 0;                                               //!< reset pulse counter
//    interrupts();                                                           //!< done changing interrupt variable(s)

	FlowMeter__currentFrequency = 0.0f;
	FlowMeter__currentDuration = 0.0f;
	FlowMeter__currentFlowrate = 0.0f;
	FlowMeter__currentVolume = 0.0f;
	FlowMeter__currentCorrection = 0.0f;

	FlowMeter__totalVolume = 0.0f;                  //!< total volume since begin of measurement (in l)
	FlowMeter__totalCorrection = 0.0f;              //!< accumulated correction factors

//    interrupts();                                                           //!< done changing interrupt variable(s)

	FlowMeter2__currentFrequency = 0.0f;
	FlowMeter2__currentDuration = 0.0f;
	FlowMeter2__currentFlowrate = 0.0f;
	FlowMeter2__currentVolume = 0.0f;
	FlowMeter2__currentCorrection = 0.0f;
	FlowMeter2__totalVolume = 0.0f;                  //!< total volume since begin of measurement (in l)
	FlowMeter2__totalCorrection = 0.0f;              //!< accumulated correction factors
}



unsigned long FlowMeter_getCurrentDuration() {
    return FlowMeter__currentDuration;                                          //!< in ms
}

double FlowMeter_getCurrentFrequency() {
    return FlowMeter__currentFrequency;                                         //!< in 1/s
}

double FlowMeter_getCurrentError() {
    /// error (in %) = error * 100
    /// error = correction rate - 1
    /// correction rate = k-factor / correction
    return (FlowMeter__properties.kFactor / FlowMeter__currentCorrection - 1) * 100;  //!< in %
}

unsigned long FlowMeter_getTotalDuration() {
    return FlowMeter__totalDuration;                                            //!< in ms
}

double FlowMeter_getTotalError() {
    /// average error (in %) = average error * 100
    /// average error = average correction rate - 1
    /// average correction rate = k-factor / corrections over time * total time
    return (FlowMeter__properties.kFactor / FlowMeter__totalCorrection * FlowMeter__totalDuration - 1) * 100;
}

void FlowMeter_setTotalDuration(unsigned long totalDuration) {
	FlowMeter__totalDuration = totalDuration;
}

void FlowMeter_setTotalVolume(double totalVolume) {
	FlowMeter__totalVolume = totalVolume;
}

void  FlowMeter_setTotalCorrection(double totalCorrection) {
	FlowMeter__totalCorrection = totalCorrection;
}

