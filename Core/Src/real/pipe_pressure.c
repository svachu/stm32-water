#include "real/pipe_pressure.h"

void PIPE_PRESSURE_init(ADC_HandleTypeDef * hadc1){
	PIPE_last_pressure = 0;
	PIPE_adc = 0;
	PIPE_adc_voltage = 0;

	PIPE2_last_pressure = 0;
	PIPE2_adc = 0;
	PIPE2_adc_voltage = 0;

	HAL_ADC_Start_DMA(hadc1, adc_value, 2);  // start the adc in dma mode
}

float PIPE_PRESSURE_measure(ADC_HandleTypeDef * hadc1){
//	HAL_ADC_Start_DMA(hadc1, adc_value, 2);  // start the adc in dma mode

//	osDelay(100);

	uint32_t adc_IN0 = 0;
	uint32_t adc_IN1 = adc_value[1];

	uint16_t samples = 10;

	for (uint16_t i = 0; i<samples ;i++){
		adc_IN0 += adc_value[0];
		osDelay(100);
	}

	adc_IN0 = adc_IN0 / samples;


//	adc_value = adc_value/samples;

	PIPE_adc = adc_IN0;
	PIPE2_adc = adc_IN1;

	// Output Voltage: 0.5-4.5 VDC
	// Working Pressure Range: 0-12 bar (0.5 - 4.5V)
	// test board: divider values  1k(1.004) 2k (1.976)
	// real board: R1 = 1k, R2 = 3k - yea i didnt have 2k laying around...
	double IN0_real_voltage = (adc_IN0 / (double)4096.0) * (double)5.0; // reference before divider (5*2)/(1+2)
	double IN1_real_voltage = (adc_IN1 / (double)4096.0) * (double)5.0;

	// Vin = (R1 + R2) * Vout / R2
	// Vin = (1 + 3) * Vout / 3
//	double IN0_calculated_voltage = (4.0 * IN0_real_voltage) / 3.0;
//	double IN1_calculated_voltage = (4.0 * IN0_real_voltage) / 3.0;

	PIPE_adc_voltage = IN0_real_voltage;
	PIPE2_adc_voltage = IN1_real_voltage;


	IN0_real_voltage -= 0.5; // minus bazal voltage
	IN1_real_voltage -= 0.5;
	// 4V .... 12 bar
	// yV .... x bar

	double pressure0 = (IN0_real_voltage * 12.0) / 4.0;
	double pressure1 = (IN1_real_voltage * 12.0) / 4.0;
	//"adc: %d, voltage: %f, pressure: %f\n", adc_value, voltage, pressure
	PIPE_last_pressure = pressure0;
	PIPE2_last_pressure = pressure1;
	return pressure0;

	/*
	 *
	 * */

}

void PIPE_PRESSURE_send(){
	// clean water
	FLOAT_TOUPLE toSend;
	toSend.first_measurement = PIPE_last_pressure;
	toSend.second_measurement = PIPE_adc;

	CAN_ID_PROPS canMessage;
	canMessage.msg_id = svachu_msg_pressure;
	canMessage.msg_type = svachu_msg_type_sensor;
	canMessage.recipient = svachu_device_svachu_king;
	canMessage.sender = svachu_device_well_control;
	canMessage.sequence = 0;
	canMessage.length = sizeof(FLOAT_TOUPLE);
	memcpy(&canMessage.data, &toSend, sizeof(FLOAT_TOUPLE));
	svachucan_send(canMessage);

	// rain water
	toSend.first_measurement = PIPE2_last_pressure;
	toSend.second_measurement = PIPE2_adc;

	canMessage.msg_id = svachu_msg_pressure_rain;
	canMessage.length = sizeof(FLOAT_TOUPLE);
	memcpy(&canMessage.data, &toSend, sizeof(FLOAT_TOUPLE));
	svachucan_send(canMessage);
}
