#include "real/flow.h"

void FLOW_init(){
	FLOW_tick_counter = 0;
	FLOW_tick_10_sec_counter = 0;
	FLOW_tick_1_min_counter = 0;
	FLOW_overall_counter = 0;

	FLOW_last_second_flow = 0;
	FLOW_last_minute_flow = 0;
	FLOW_last_consumption = 0;

	FlowMeter(UncalibratedSensor);
}

void FLOW_tick(){
	FlowMeter_count();
}


float FLOW_per_second(){
	FlowMeter_tick(1000);

	return FlowMeter_getCurrentFlowrate();
}

float FLOW_overall(){
	return FlowMeter_getTotalVolume();
}

void FLOW_send_flow_data(){
	// clean water
	FLOAT_TOUPLE toSend;
	toSend.first_measurement = FlowMeter__currentFlowrate;
	toSend.second_measurement = FlowMeter__currentVolume;

	CAN_ID_PROPS canMessage;
	canMessage.msg_id = svachu_msg_flow;
	canMessage.msg_type = svachu_msg_type_sensor;
	canMessage.recipient = svachu_device_svachu_king;
	canMessage.sender = svachu_device_well_control;
	canMessage.sequence = 0;
	canMessage.length = sizeof(FLOAT_TOUPLE);
	memcpy(&canMessage.data, &toSend, sizeof(FLOAT_TOUPLE));
	svachucan_send(canMessage);

	// rain water
	toSend.first_measurement = FlowMeter2__currentFlowrate;
	toSend.second_measurement = FlowMeter2__currentVolume;
	canMessage.msg_id = svachu_msg_flow_rain;
	canMessage.length = sizeof(FLOAT_TOUPLE);
	memcpy(&canMessage.data, &toSend, sizeof(FLOAT_TOUPLE));
	svachucan_send(canMessage);

}

