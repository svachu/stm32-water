#include "real/oled.h"

void OLED_init(){
//	uint8_t check = SSD1306_Init ();  // initialize the diaply
//	SSD1306_Fill (0);  // fill the display with black color
//	SSD1306_UpdateScreen(); // update screen
//	SSD1306_GotoXY (10,10);  // goto 10, 10
//	SSD1306_Puts ("Hello", &Font_16x26, 1);  // print Hello
//	SSD1306_GotoXY (10, 30);
//	SSD1306_Puts ("3.136 bar", &Font_16x26, 1);
//	SSD1306_DrawRectangle(0, 1, SSD1306_WIDTH, SSD1306_HEIGHT, SSD1306_COLOR_WHITE);
//	SSD1306_UpdateScreen(); // update screen

	oled_init();
	OLED_clear();
//	graphics_text(0, 32, FONT_SEVEN_DOT, "prdel");
//	graphics_text(0, 0*LINE_SIZE, FONT_SEVEN_DOT, "Milacek ZUZANKA!");
//	graphics_text(0, 1*LINE_SIZE, FONT_SEVEN_DOT, "Svahcudata");
//	graphics_text(0, 2*LINE_SIZE, FONT_SEVEN_DOT, "653 bar");
//	graphics_text(0, 3*LINE_SIZE, FONT_SEVEN_DOT, "123 bar");
	oled_update();


}

void OLED_loop(){
	osDelay(100);
	OLED_show_pressure(PIPE_last_pressure);
	OLED_show_flow(FlowMeter__currentFlowrate, FLOW_last_minute_flow, FlowMeter__totalVolume);
	OLED_show_debug();
	oled_update();
}

void OLED_clear(){
	invert_rectangle(0, 0, 128, 64);
	invert_rectangle(0, 0, 128, 64);
	oled_update();
}

void OLED_show_flow(float flow_per_sec, float flow_per_minute, float flow_overall){
//	invert_rectangle(0, 0, 128, 3*LINE_SIZE);

//	char buffer[50];
//	uint32_t per_sec_num = flow_per_sec;
//	uint32_t per_sec_dec = (flow_per_sec-per_sec_num)*1000;
//	snprintf(buffer, 100, "%lu.%03lu l/sec", per_sec_num, per_sec_dec); // takes 8kB of FLASH :D Nice
//	invert_rectangle(0, 0*LINE_SIZE, 7, 64); // erase that line first
//	graphics_text(0, 0*LINE_SIZE, FONT_SEVEN_DOT, buffer);

	// print first flow
	OLED_print_float(FlowMeter__currentFlowrate, 0, 0, "l/s");
	OLED_print_uint(FlowMeter__currentPulses, 0, 75, "IN");

	// print second flow
	OLED_print_float(FlowMeter2__currentFlowrate, 1, 0, "l/s");
	OLED_print_uint(FlowMeter2__currentPulses, 1, 75, "IN");

	// total volume
	OLED_print_float(FlowMeter__totalVolume, 2, 0, "l");
	OLED_print_float(FlowMeter2__totalVolume, 2, 75, "l");


//	snprintf(buffer, 100, "%f l/min", flow_per_minute);
//	graphics_text(0, 1*LINE_SIZE, FONT_SEVEN_DOT, buffer);
//	snprintf(buffer, 100, "%f l", flow_overall);
//	graphics_text(0, 2*LINE_SIZE, FONT_SEVEN_DOT, buffer);



}
void OLED_show_pressure(float pressure){
//	invert_rectangle(0, 3*LINE_SIZE, 128, 32);


	OLED_print_float(PIPE_last_pressure, 3, 0, "bar");
	OLED_print_float(PIPE_adc_voltage, 4, 0, "V");
	OLED_print_uint(PIPE_adc, 4, 75, "");


	OLED_print_float(PIPE2_last_pressure, 5, 0, "bar");
	OLED_print_float(PIPE2_adc_voltage, 6, 0, "V");
	OLED_print_uint(PIPE2_adc, 6, 75, "");




//	char buffer[50];
//	float pressureZlg = pressure;
//	if (pressure < 0){
//		pressure = fabs(pressure); // abs
//	}
//	int32_t pressure_num = pressure;
//	uint32_t pressure_dec = (pressure-pressure_num)*1000;
//
//	if (pressureZlg < 0) {
//		pressure_num -= pressure_num*2;
//	}
//
//	snprintf(buffer, 50, "%ld.%03lu bar", pressure_num , pressure_dec); // takes 8kB of FLASH :D Nice
//	graphics_text(0, 3*LINE_SIZE, FONT_SEVEN_DOT, buffer);


}

void OLED_show_debug(){
//	char buffer[50];
//
//	snprintf(buffer, 50, "%05ld", FLOW_tick_counter + FLOW_overall_counter);
//	graphics_text(75, 0*LINE_SIZE, FONT_SEVEN_DOT, buffer);
//
//	snprintf(buffer, 50, "%04ld adc", PIPE_adc);
//	graphics_text(75, 2*LINE_SIZE, FONT_SEVEN_DOT, buffer);
//
//	float voltage = PIPE_adc_voltage;
//	float voltageZlg = PIPE_adc_voltage;
//	if (voltage < 0){
//		voltage = fabs(voltage); // abs
//	}
//	int32_t voltage_num = voltage;
//	uint32_t voltage_dec = (voltage-voltage_num)*1000;
//
//	if (voltageZlg < 0) {
//		voltage_num -= voltage_num*2;
//	}
//
//	snprintf(buffer, 50, "%ld.%03lu V", voltage_num , voltage_dec); // takes 8kB of FLASH :D Nice
//	graphics_text(75, 3*LINE_SIZE, FONT_SEVEN_DOT, buffer);
//





}

void OLED_print_float(float toPrint, uint8_t line, uint8_t x, const char * unit){
	char buffer[50];
	uint32_t decimal = toPrint;
	uint32_t factor = (toPrint-decimal)*1000;

	snprintf(buffer, 50, "%lu.%03lu %s", decimal, factor, unit); // takes 8kB of FLASH :D Nice
	rectangle(x, line*LINE_SIZE, 128, (line+1)*LINE_SIZE); // erase that line first
	graphics_text(x, line*LINE_SIZE, FONT_SEVEN_DOT, buffer);
}


void OLED_print_uint(uint32_t toPrint, uint8_t line, uint8_t x, const char * unit){
	char buffer[50];

	snprintf(buffer, 50, "%lu %s", toPrint, unit); // takes 8kB of FLASH :D Nice
	rectangle(x, line*LINE_SIZE, 128, (line+1)*LINE_SIZE); // erase that line first
	graphics_text(x, line*LINE_SIZE, FONT_SEVEN_DOT, buffer);
}

void OLED_show_uptime(uint32_t uptime){

}
