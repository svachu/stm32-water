#ifndef CANSVACHUCOL_HPP_
#define CANSVACHUCOL_HPP_


enum SVACHU_MSG_TYPE {
	svachu_msg_type_sensor  = 0,
	svachu_msg_type_control = 1,
	svachu_msg_type_error   = 2,
	svachu_msg_type_alarm   = 3,
	svachu_msg_type_info    = 4,
	svachu_msg_type_ack     = 5
};

typedef enum SVACHU_MSG_TYPE SVACHU_MSG_TYPE;

enum SVACHU_MSG {  // 128 addresses
	// all devices can
	svachu_msg_ping       = 0,
	svachu_msg_blink      = 1,
	svachu_msg_power      = 2,
	svachu_msg_uptime     = 3,
	svachu_msg_reserved   = 4,
	svachu_msg_reserved_2 = 5,
	svachu_msg_reserved_3 = 6,
	svachu_msg_reserved_4 = 7,
	svachu_msg_reserved_5 = 8,

	// standard messages
	svachu_msg_cpu                 = 9,
	svachu_msg_mem                 = 10,
	svachu_msg_cpu_temp            = 11,
	svachu_msg_mains_consumption   = 12,
	svachu_msg_mains_voltage       = 13,
	svachu_msg_switch_relay        = 14,
	svachu_msg_switch_relay_states = 15,
	svachu_msg_flow                = 16,
	svachu_msg_pressure            = 17,
	svachu_msg_well_depth          = 18,
	svachu_msg_well_top_switch     = 19,
	svachu_msg_well_min_switch     = 20,
	svachu_msg_well_temp           = 21,
	svachu_msg_well_pump           = 22,
	svachu_msg_change_depth        = 23,
	svachu_msg_esp_now_msg         = 24,
	svachu_msg_esp_now_req         = 25,
	svachu_msg_remote_touched      = 26,
	svachu_msg_lock                = 27,
	svachu_msg_alarm               = 28,
	svachu_msg_movement            = 29,
	svachu_msg_env_co              = 30,
	svachu_msg_env_co2             = 31,
	svachu_msg_env_C               = 32,
	svachu_msg_env_bar             = 33,
	svachu_msg_env_humi            = 34,
	svachu_msg_switch_consumption  = 35,
	svachu_msg_switch_cut_line     = 36,
	svachu_msg_cam_data            = 37,
	svachu_msg_flow_rain           = 38,
	svachu_msg_pressure_rain       = 39,

	// till 64

	// every device can use:
	svachu_msg_kill_switch        = 64,
	svachu_msg_error              = 65,
	svachu_msg_error_critical     = 66,
	svachu_msg_tampering          = 67,
	svachu_msg_state_change       = 68,
	svachu_msg_info_string        = 69,
	svachu_msg_debug_string       = 70



};
typedef enum SVACHU_MSG SVACHU_MSG;

enum SVACHU_DEVICE { // 64 addresses
	svachu_device_BROADCAST		    = 0,
	svachu_device_svachu_king	    = 1,
	svachu_device_svachu_king_2	    = 2,
	svachu_device_relayer		    = 3,
	svachu_device_relayer_2		    = 4,
	svachu_device_well_control	    = 5,
	svachu_device_well_control_2    = 6,
	svachu_device_well_probe	    = 7,
	svachu_device_well_probe_2	    = 8,
	svachu_device_esp_repeater	    = 9,
	svachu_device_remote		    = 10,
	svachu_device_remote_2		    = 11,
	svachu_device_buzzer		    = 12,
	svachu_device_buzzer_2		    = 13,
	svachu_device_movement		    = 14,
	svachu_device_movement_2	    = 15,
	svachu_device_movement_3	    = 16,
	svachu_device_movement_4	    = 17,
	svachu_device_weather		    = 18,
	svachu_device_weather_2		    = 19,
	svachu_device_weather_3		    = 20,
	svachu_device_can_switch	    = 21,
	svachu_device_can_switch_2	    = 22,
	svachu_device_cam			    = 23,
	svachu_device_cam_2			    = 24,
	svachu_device_cam_3			    = 25,
	svachu_device_doorReed		    = 26,
	svachu_device_door			    = 27,
	svachu_device_door_2		    = 28,
	svachu_device_door_3            = 29,
	svachu_device_gsm               = 30,

	// those addresses are used even in espNow data packet (if supported :D)
	// id part of espNow packet looks line $32 -> means weather_outside
	svachu_device_wireless_weather_inside   = 31,
	svachu_device_wireless_weather_outside  = 32,
	svachu_device_wireless_weather_cellar   = 33,
	svachu_device_wireless_weather_bedroom  = 34,
	svachu_device_wireless_weather_bathroom = 35,
	svachu_device_wireless_level_red        = 36,
	svachu_device_wireless_level_blue       = 37
};

typedef enum SVACHU_DEVICE SVACHU_DEVICE;

// CAN MESSAGE
typedef struct {
	SVACHU_MSG_TYPE msg_type;
	SVACHU_MSG msg_id;
	SVACHU_DEVICE recipient;
	SVACHU_DEVICE sender;
	uint8_t sequence;
	uint8_t length;
	uint8_t data[8];
} CAN_ID_PROPS;

#endif
