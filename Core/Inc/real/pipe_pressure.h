#ifndef PIPE_PRESSURE_H_
#define PIPE_PRESSURE_H_

#include "common.h"

float PIPE_last_pressure;
uint32_t PIPE_adc;
float PIPE_adc_voltage;

float PIPE2_last_pressure;
uint32_t PIPE2_adc;
float PIPE2_adc_voltage;

uint32_t adc_value[2];

void PIPE_PRESSURE_init(ADC_HandleTypeDef * hadc1);
float PIPE_PRESSURE_measure(ADC_HandleTypeDef * hadc1);
void PIPE_PRESSURE_send();


#endif

