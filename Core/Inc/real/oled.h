#ifndef OLED_H_
#define OLED_H_

#include "common.h"
//#include "ssd1306.h"
//#include "fonts.h"
//#include "test.h"
#include <math.h>
#include "../ssd1306/fonts.h"
#include "../ssd1306/gfx.h"
#include "../ssd1306/oled.h"

#define LINE_SIZE 8


void OLED_init();
void OLED_loop();
void OLED_show_flow(float flow_per_sec, float flow_per_minute, float flow_overall);
void OLED_show_pressure(float pressure);
void OLED_show_uptime(uint32_t uptime);
void OLED_show_debug();
void OLED_print_float(float number, uint8_t line, uint8_t x, const char * unit);
void OLED_print_uint(uint32_t number, uint8_t line, uint8_t x, const char * unit);
void OLED_clear();


#endif

