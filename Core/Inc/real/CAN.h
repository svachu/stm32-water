#ifndef CAN_HPP_
#define CAN_HPP_

#include "common.h"


#define CAN_STD_ID 0b1111111
#define CAN_EXT_ID 0b1111111

CAN_TxHeaderTypeDef   CAN_TxHeader;
CAN_RxHeaderTypeDef   CAN_RxHeader;
uint8_t               CAN_RxData[8];
uint32_t              TxMailbox;
CAN_HandleTypeDef     * CAN_ref;

void CAN_init(CAN_HandleTypeDef *hcan);
void CAN_setFilter(uint32_t id, uint32_t mask, uint8_t bank);
void CAN_send(uint32_t id, unsigned char * data, uint8_t len, uint8_t request);
void CAN_receive_loop();

#endif

