#ifndef COMMON_HPP_
#define COMMON_HPP_

#include "cmsis_os.h"
#include "main.h"
#include "../main.h"
#include "can_svachucol.h"
#include "svachucan.h"

#include "CAN.h"
#include "flow.h"
#include "pipe_pressure.h"
#include "oled.h"
#include "FlowMeter.h"

SVACHU_DEVICE MY_CAN_ADDR; // my addr is GSM

void COMMON_init();

#endif
