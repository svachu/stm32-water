#ifndef FLOWMETEROS_H_
#define FLOWMETEROS_H_

#define min(a,b) (((a) < (b)) ? (a) : (b))
#define max(a,b) (((a) > (b)) ? (a) : (b))

#include <math.h>

typedef struct {
  double capacity;      //!< capacity, upper limit of flow rate (in l/min)
  double kFactor;       //!< "k-factor" (in (pulses/s) / (l/min)), e.g.: 1 pulse/s = kFactor * l/min
  double mFactor[10];   //!< multiplicative correction factor near unity, "meter factor" (per decile of flow)
} FlowSensorProperties;

extern FlowSensorProperties UncalibratedSensor; //!< default sensor
extern FlowSensorProperties FS300A;             //!< see documentation about FS300A/SEN02141B
extern FlowSensorProperties FS400A;             //!< see documentation about FS400A/USN-HS10TA

/**
 * FlowMeter
 */
void FlowMeter(FlowSensorProperties prop);

double FlowMeter_getCurrentFlowrate();                 //!< Returns the current flow rate since last tick (in l/min).
double FlowMeter_getCurrentVolume();                   //!< Returns the current volume since last tick (in l).
double FlowMeter_getTotalFlowrate();                   //!< Returns the (linear) average flow rate in this flow meter instance (in l/min).
double FlowMeter_getTotalVolume();                     //!< Returns the total volume flown trough this flow meter instance (in l).
void FlowMeter_tick(unsigned long duration);
void FlowMeter_count();                                //!< Increments the internal pulse counter. Serves as an interrupt callback routine.
void FlowMeter_reset();                                //!< Prepares the flow meter for a fresh measurement. Resets all current values, but not the totals.
/*
 * setters enabling continued metering across power cycles
 */
void FlowMeter_setTotalDuration(unsigned long totalDuration); //!< Sets the total (overall) duration (i.e. after power up).
void FlowMeter_setTotalVolume(double totalVolume);            //!< Sets the total (overall) volume (i.e. after power up).
void FlowMeter_setTotalCorrection(double totalCorrection);    //!< Sets the total (overall) correction factor (i.e. after power up).

unsigned long FlowMeter_getCurrentDuration();          //!< Returns the duration of the current tick (in ms).
double FlowMeter_getCurrentFrequency();                //!< Returns the pulse rate in the current tick (in 1/s).
double FlowMeter_getCurrentError();                    //!< Returns the error resulting from the current measurement (in %).

unsigned long FlowMeter_getTotalDuration();            //!< Returns the total run time of this flow meter instance (in ms).
double FlowMeter_getTotalError();                      //!< Returns the (linear) average error of this flow meter instance (in %).


FlowSensorProperties FlowMeter__properties;            //!< sensor properties (including calibration data)

unsigned long FlowMeter__currentDuration;              //!< current tick duration (convenience, in ms)
double FlowMeter__currentFrequency;                    //!< current pulses per second (convenience, in 1/s)
double FlowMeter__currentFlowrate;              //!< current flow rate (in l/tick), e.g.: 1 l / min = 1 pulse / s / (pulses / s / l / min)
double FlowMeter__currentVolume;                //!< current volume (in l), e.g.: 1 l = 1 (l / min) / (60 * s)
double FlowMeter__currentCorrection;                   //!< currently applied correction factor
unsigned long FlowMeter__totalDuration;         //!< total measured duration since begin of measurement (in ms)
double FlowMeter__totalVolume;                  //!< total volume since begin of measurement (in l)
double FlowMeter__totalCorrection;              //!< accumulated correction factors

volatile unsigned long FlowMeter__currentPulses;   //!< pulses within current sample period




FlowSensorProperties FlowMeter2__properties;            //!< sensor properties (including calibration data)
unsigned long FlowMeter2__currentDuration;              //!< current tick duration (convenience, in ms)
double FlowMeter2__currentFrequency;                    //!< current pulses per second (convenience, in 1/s)
double FlowMeter2__currentFlowrate;              //!< current flow rate (in l/tick), e.g.: 1 l / min = 1 pulse / s / (pulses / s / l / min)
double FlowMeter2__currentVolume;                //!< current volume (in l), e.g.: 1 l = 1 (l / min) / (60 * s)
double FlowMeter2__currentCorrection;                   //!< currently applied correction factor
unsigned long FlowMeter2__totalDuration;         //!< total measured duration since begin of measurement (in ms)
double FlowMeter2__totalVolume;                  //!< total volume since begin of measurement (in l)
double FlowMeter2__totalCorrection;              //!< accumulated correction factors

volatile unsigned long FlowMeter2__currentPulses;   //!< pulses within current sample period


#endif   // FLOWMETER_H
