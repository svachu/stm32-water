#ifndef SVACHUCAN_HPP_
#define SVACHUCAN_HPP_

#include "common.h"


void svachucan_send(CAN_ID_PROPS messageProps);
void svachuchan_receive(CAN_RxHeaderTypeDef * canHeader, uint8_t * data);
void svachucan_determine(CAN_ID_PROPS messageProps);

#endif
