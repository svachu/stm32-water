#ifndef FLOW_H_
#define FLOW_H_

#include "common.h"


#define TICKS_PER_LITER 400

typedef struct {
	float first_measurement;
	float second_measurement;
} FLOAT_TOUPLE;

uint32_t FLOW_tick_counter;
uint32_t FLOW_tick_10_sec_counter;
uint32_t FLOW_tick_1_min_counter;
uint32_t FLOW_overall_counter;

float FLOW_last_second_flow;
float FLOW_last_minute_flow;
float FLOW_last_consumption;


void FLOW_init();
void FLOW_tick();
float FLOW_per_second();
float FLOW_per_ten_seconds();
float FLOW_per_minute();
float FLOW_overall();

void FLOW_send_flow_data();


#endif

